import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';

import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
	componentWillMount() {
		const config = {
			apiKey: 'AIzaSyBjhfVGmWJLrCDA5WGkrlFiOVOqIW8hWPQ',
			authDomain: 'manager-d275f.firebaseapp.com',
			databaseURL: 'https://manager-d275f.firebaseio.com',
			projectId: 'manager-d275f',
			storageBucket: '',
			messagingSenderId: '507899343247'
		};
		firebase.initializeApp(config);
	}

	render() {
		store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
		return (
			<Provider store={store}>
				<Router />
			</Provider>
		)
	}
}

export default App;